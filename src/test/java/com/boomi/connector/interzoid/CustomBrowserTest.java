// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.interzoid;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.openapi.OpenAPIBrowser;
import com.boomi.connector.testutil.SimpleAtomConfig;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.LogUtil;
import org.junit.Test;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomBrowserTest {

    private static final Logger LOG = LogUtil.getLogger(CustomBrowserTest.class);

    int endPointCount = 0;
    int errorCount = 0;
    int stackoverflowCount = 0;
    int otherErrorCount = 0;
    ArrayList<String> erroringOperationIds = new ArrayList<>();

    final String[] SPECS = {
            "https://oas.interzoid.com/api/getcompanymatchadvanced.json",
            "https://oas.interzoid.com/api/getfullnamematch.json",
            "https://oas.interzoid.com/api/getfullnameparsedmatch.json",
            "https://oas.interzoid.com/api/getaddressmatchadvanced.json",
            "https://oas.interzoid.com/api/getcitymatch.json",
            "https://oas.interzoid.com/api/getcountrymatch.json",
            "https://oas.interzoid.com/api/getstateabbreviation.json",
            "https://oas.interzoid.com/api/getcitystandard.json",
            "https://oas.interzoid.com/api/getcountrystandard.json",
            "https://oas.interzoid.com/api/getemailinfo.json",
            "https://oas.interzoid.com/api/globalpageload.json",
            "https://oas.interzoid.com/api/getcurrencyrate.json",
            "https://oas.interzoid.com/api/convertcurrency.json",
            "https://oas.interzoid.com/api/getcryptoprice.json",
            "https://oas.interzoid.com/api/getmetalprice.json",
            "https://oas.interzoid.com/api/getglobaltime.json",
            "https://oas.interzoid.com/api/getglobalnumberinfo.json",
            "https://oas.interzoid.com/api/lookupareacode.json",
            "https://oas.interzoid.com/api/getareacodefromnumber.json",
            "https://oas.interzoid.com/api/getweathercity.json",
            "https://oas.interzoid.com/api/getweatherzip.json",
            "https://oas.interzoid.com/api/getweatherlatlong.json",
            "https://oas.interzoid.com/api/getzipinfo.json"
    };

    final String[] httpMethods = {
            "GET",
            "POST",
            "PUT",
            "DELETE",
            "PATCH",
            "HEAD",
            "OPTIONS",
            "TRACE"
    };

    @Test(expected = Test.None.class)
    public void testTypes() {
        CustomConnector connector = new CustomConnector();
        for (String api : SPECS) {
            Map<String, Object> connProps = new HashMap<>();
            connProps.put("spec", api);

            for (String httpMethod : httpMethods) {
                SimpleBrowseContext browseContext = new SimpleBrowseContext(
                        new SimpleAtomConfig(),
                        connector,
                        null,
                        httpMethod,
                        connProps,
                        null
                );

                OpenAPIBrowser browser = (OpenAPIBrowser) connector.createBrowser(browseContext);
                try {
                    browser.getObjectTypes();
                } catch (Exception e) {
                    if (e.getMessage() == null || !e.getMessage().contains("no types available")) {
                        throw e;
                    }
                }
            }
        }
    }

    @Test(expected = Test.None.class)
    public void testProfiles() {

        LOG.log(Level.INFO, "starting testprofiles\n*******************");

        for (String api : SPECS) {
            endPointCount = 0;
            errorCount = 0;
            stackoverflowCount = 0;
            otherErrorCount = 0;

            for (String httpMethod : httpMethods) {
                browseHttpMethod(httpMethod, api);
            }

            String message = String.format("API: %s", api);
            LOG.log(Level.INFO, message);
            message = String.format("%d out of %d endpoints failed.", errorCount, endPointCount);
            LOG.log(Level.INFO, message);
            if (errorCount > 0) {
                message = String.format("%d of those are stackoverflow errors.", stackoverflowCount);
                LOG.log(Level.INFO, message);
                message = String.format("%d of those are other errors.", otherErrorCount);
                LOG.log(Level.INFO, message);
            }
            LOG.log(Level.INFO, "***************************************************");
        }
    }

    private void browseHttpMethod(String httpMethod, String api) {
        Map<String, Object> connProps = new HashMap<>();
        connProps.put("spec", api);
        CustomConnector connector = new CustomConnector();
        SimpleBrowseContext browseContext = new SimpleBrowseContext(
                new SimpleAtomConfig(),
                connector,
                null,
                httpMethod,
                connProps,
                null
        );

        OpenAPIBrowser browser = (OpenAPIBrowser) connector.createBrowser(browseContext);
        ObjectTypes objectTypes = new ObjectTypes();
        try {
            objectTypes = browser.getObjectTypes();
        } catch (Exception e) {
            if (e.getMessage() == null || !e.getMessage().contains("no types available")) {
                throw e;
            }
        }
        for (ObjectType objectType : objectTypes.getTypes()) {
            endPointCount++;
            String path = objectType.getId();
            String operationId = objectType.getLabel();
            Set<ObjectDefinitionRole> roles = new HashSet<>();
            roles.add(ObjectDefinitionRole.INPUT);
            roles.add(ObjectDefinitionRole.OUTPUT);

            try {
                browser.getObjectDefinitions(path, roles);
            } catch (StackOverflowError e) {
                String message = String.format("Stackoverflow error for operationId %s", operationId);
                LOG.log(Level.WARNING, message);
                erroringOperationIds.add(operationId);
                errorCount++;
                stackoverflowCount++;
            } catch (Exception e) {
                String message = String.format(
                        "Browser failing for path: %s, http method: %s, operation id: %s %n error: %s",
                        path, httpMethod, operationId, e.getMessage());
                LOG.log(Level.WARNING, message);
                erroringOperationIds.add(operationId);
                errorCount++;
                otherErrorCount++;
            }
        }
    }

}
